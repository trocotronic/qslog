// Copyright (c) 2013, Razvan Petru
// All rights reserved.

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, this
//   list of conditions and the following disclaimer in the documentation and/or other
//   materials provided with the distribution.
// * The name of the contributors may not be used to endorse or promote products
//   derived from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef QSLOG_H
#define QSLOG_H

#include "QsLogLevel.h"
#include "QsLogDest.h"
#include "QsLogMessage.h"
#include <QtCore/QDebug>
#include <QtCore/QString>

#define QS_LOG_VERSION "2.1"

namespace QsLogging
{

class Destination;
class LoggerImpl; // d pointer

class QSLOG_SHARED_OBJECT Logger
{
public:
    static Logger& instance();
    static Level levelFromLogMessage(const QString& logMessage, bool* conversionSucceeded = 0);

    ~Logger();

#if defined(Q_OS_WIN)
    //! When QS_LOG_SEPARATE_THREAD is defined on Windows, and you are using this library as a DLL,
    //! this function must be called before your program ends, to ensure a clean shutdown of the logger thread.
    //! Failing to call it will result in an assert being triggered, an error message being printed
    //! out and most probably a deadlock.
    //! Returns the wait result for the thread. When called on a non-threaded logger returns true
    //! immediately.
    bool shutDownLoggerThread();
#endif

    //! Adds a log message destination. Don't add null destinations.
    void addDestination(DestinationPtr destination);

    //! Removes a previously added destination. Does nothing if destination was not previously added.
    void removeDestination(const DestinationPtr& destination);
    
    void clearDestinations();

    //! Checks if a destination of a specific type has been added. Pass T::Type as parameter.
    bool hasDestinationOfType(const char* type) const;

    //! Logging at a level < 'newLevel' will be ignored
    void setLoggingLevel(Level newLevel);

    //! The default level is INFO
    Level loggingLevel() const;

    //! The helper forwards the streaming to QDebug and builds the final
    //! log message.
    class QSLOG_SHARED_OBJECT Helper
    {
    public:
        explicit Helper(Level logLevel, const char *file, int line, const char *function) :
            level(logLevel),
            qtDebug(&buffer),
            context(file, line, function, "default") {}
        ~Helper();
        QDebug& stream(){ return qtDebug; }
        void stream(const char *msg, ...) 
        {
            va_list ap;
            va_start(ap, msg); // use variable arg list
            buffer = QString::vasprintf(msg, ap);
            va_end(ap);
        }

    private:
        Level level;
        QString buffer;
        QDebug qtDebug;
        QMessageLogContext context;
    };

private:
    Logger();
    Logger(const Logger&);            // not available
    Logger& operator=(const Logger&); // not available

    void enqueueWrite(const LogMessage& message);
    void write(const LogMessage& message);

    LoggerImpl* d;

    friend class LoggerThread;
};

} // end namespace

//! Logging macros: define QS_LOG_LINE_NUMBERS to get the file and line number
//! in the log output.
#ifdef QS_LOG_LINE_NUMBERS
#define QT_MESSAGELOGCONTEXT
#endif

#define QLOG_TRACE QsLogging::Logger::Helper(QsLogging::TraceLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream
#define QLOG_DEBUG QsLogging::Logger::Helper(QsLogging::DebugLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream
#define QLOG_INFO QsLogging::Logger::Helper(QsLogging::InfoLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream
#define QLOG_WARN QsLogging::Logger::Helper(QsLogging::WarnLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream
#define QLOG_ERROR QsLogging::Logger::Helper(QsLogging::ErrorLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream
#define QLOG_FATAL QsLogging::Logger::Helper(QsLogging::FatalLevel,QT_MESSAGELOG_FILE,QT_MESSAGELOG_LINE,QT_MESSAGELOG_FUNC).stream

#ifdef QS_LOG_DISABLE
#include "QsLogDisableForThisFile.h"
#endif

#endif // QSLOG_H
