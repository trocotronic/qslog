#include "QsLogLevel.h"
#include <QString>
#include <QObject>
#include <cassert>

QString QsLogging::LocalizedLevelName(QsLogging::Level theLevel)
{
    switch (theLevel) {
    case QsLogging::TraceLevel:
        return QObject::tr("Trace");
    case QsLogging::DebugLevel:
        return QObject::tr("Debug");
    case QsLogging::InfoLevel:
        return QObject::tr("Info");
    case QsLogging::WarnLevel:
        return QObject::tr("Warning");
    case QsLogging::ErrorLevel:
        return QObject::tr("Error");
    case QsLogging::FatalLevel:
        return QObject::tr("Fatal");
    default:
        return QString();
    }
}
